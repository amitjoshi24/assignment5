#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify, make_response
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]


@app.route('/book/JSON/')
def bookJSON():
    # <your code>
    print (str(jsonify(books)))
    return make_response(jsonify(books), 400)

@app.route('/')
@app.route('/book/')
def showBook():
    # <your code>
    return render_template('index.html', books=books)


@app.route('/book/newing', methods=['GET', 'POST'])
def newing():
	newBookName = request.form['newBookName']
	newBook = dict()
	newBook['title'] = newBookName

	maxID = -1
	for book in books:
		maxID = max(maxID, int(book['id']))
	taken = [False] * (maxID)


	print ("lenTaken: " + str(len(taken)))

	for book in books:
		print ("index tryna access: " + str(int(book['id']) - 1))
		taken[int(book['id']) - 1] = True

	newID = len(books) + 1

	# searches for a open spot
	for i in range(len(taken)):
		if taken[i] == False:
			newID = i + 1
			break

	newBook['id'] = str(newID)


	books.append(newBook)
	return redirect('/book/')

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
    return render_template('newBook.html', books=books)

@app.route('/book/<int:book_id>/editing', methods = ['GET', 'POST'])
def editing(book_id):
    newName = request.form['newName']
    books[book_id-1]['title'] = newName
    return redirect('/book/')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	return render_template('editBook.html', books=books, book_id=book_id-1)

@app.route('/book/<int:book_id>/deleting', methods = ['GET', 'POST'])
def deleting(book_id):
    del books[book_id-1]
    return redirect('/book/')

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # <your code>
    return render_template('deleteBook.html', books=books, book_id=book_id-1)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

